package ec.edu.utpl.eessi;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-07-24T19:06:58")
@StaticMetamodel(SolicitudesPendientes.class)
public class SolicitudesPendientes_ { 

    public static volatile SingularAttribute<SolicitudesPendientes, String> apellidos;
    public static volatile SingularAttribute<SolicitudesPendientes, String> metaInstitutionName;
    public static volatile SingularAttribute<SolicitudesPendientes, Long> ticket;
    public static volatile SingularAttribute<SolicitudesPendientes, String> institutionName;
    public static volatile SingularAttribute<SolicitudesPendientes, String> metaNombres;
    public static volatile SingularAttribute<SolicitudesPendientes, String> metaApellidos;
    public static volatile SingularAttribute<SolicitudesPendientes, String> nombres;

}