/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi.util;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author utpl
 */
@XmlRootElement
public class Relation implements Serializable{
    private String description;
    private Datacode datacodde;

    public Relation() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Datacode getDatacodde() {
        return datacodde;
    }

    public void setDatacodde(Datacode datacodde) {
        this.datacodde = datacodde;
    }
    
    
}
