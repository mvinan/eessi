/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi.util;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author utpl
 */
@XmlRootElement
public class Response implements Serializable{
    private String eessim_error_code;
    private String eessim_error_desc;
    private String value;

    public Response() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEessim_error_code() {
        return eessim_error_code;
    }

    public void setEessim_error_code(String eessim_error_code) {
        this.eessim_error_code = eessim_error_code;
    }

    public String getEessim_error_desc() {
        return eessim_error_desc;
    }

    public void setEessim_error_desc(String eessim_error_desc) {
        this.eessim_error_desc = eessim_error_desc;
    }
    
    
    
    
}
