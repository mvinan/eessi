/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi.util;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author utpl
 */

@XmlRootElement
public class Metadata implements Serializable{
    private Datacode datacode;
    private String dataname;
    private CreatorInstitution creator_institution;
    private SendingInstitution sending_institution;
    private Dataset dataset;
    private String description;
    private String date;
    private String format;
    private Language language;
    private Relation relation;
    private Scope scope;
    private String mandate;
    private References references;

    public Metadata() {
    }
    
    
    
    
    public Datacode getDatacode() {
        return datacode;
    }

    public void setDatacode(Datacode datacode) {
        this.datacode = datacode;
    }

    public String getDataname() {
        return dataname;
    }

    public void setDataname(String dataname) {
        this.dataname = dataname;
    }

    public CreatorInstitution getCreator_institution() {
        return creator_institution;
    }

    public void setCreator_institution(CreatorInstitution creator_institution) {
        this.creator_institution = creator_institution;
    }

    public SendingInstitution getSending_institution() {
        return sending_institution;
    }

    public void setSending_institution(SendingInstitution sending_institution) {
        this.sending_institution = sending_institution;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public String getMandate() {
        return mandate;
    }

    public void setMandate(String mandate) {
        this.mandate = mandate;
    }

    public References getReferences() {
        return references;
    }

    public void setReferences(References references) {
        this.references = references;
    }
    
    
    
}
