/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi.util;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author utpl
 */
@XmlRootElement
public class References implements Serializable{
    private String id;
    private String rdfSyntax;

    public References() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRdfSyntax() {
        return rdfSyntax;
    }

    public void setRdfSyntax(String rdfSyntax) {
        this.rdfSyntax = rdfSyntax;
    }
    
    
}
