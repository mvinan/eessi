/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import ec.edu.utpl.eessi.util.Metadata;
import ec.edu.utpl.eessi.util.UtilEessi;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author utpl
 */
@Path(value = "/metadata")
public class MetadataWS {
    
    
    
    
    
    @GET
    @Path(value = "/settingUpMetadata")
    @Produces(MediaType.APPLICATION_JSON)
    public Response settingUpMetadata(@QueryParam("field") String field) {
        System.out.println(field + "*********************");
        try {
            Metadata metadata = new Metadata();
            metadata.setCreator_institution(null);
            metadata.setDatacode(null);
            metadata.setDataname(field);
            metadata.setDate(field);
            metadata.setDescription(field);
            metadata.setFormat(field);
            metadata.setLanguage(null);
            metadata.setMandate(field);
            metadata.setReferences(null);
            metadata.setRelation(null);
            metadata.setScope(null);
            metadata.setSending_institution(null);
            String res = "{\n"
                    + "  \"@context\": {\n"
                    + "    \"pob\": \"http://www.cc.uah.es/hilera/essim/place-of-birth.rdf#\",\n"
                    + "    \"essim\": \"http://essim.org/ns/essim#\",\n"
                    + "    \"got\": \"http://got.org/resource#\",\n"
                    + "    \"dc\": \"http://purl.org/dc/elements/1.1/\",\n"
                    + "    \"rdf\": \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
                    + "  },\n"
                    + "  \"@graph\": [	\n"
                    + "	{\n"
                    + "      \"@id\": \"pob:PlaceOfBirth\",\n"
                    + "      \"@type\": \"essim:Field\",\n"
                    + "      \"essim:relation\": {\"@id\":\"_b0\"},\n"
                    + "      \"essim:language\": {\"@id\": \"http://dbpedia.org/page/English_language#iso6391Code\", \"essim:language\": \"en\"},\n"
                    + "      \"essim:description\": \"State the place of birth\",\n"
                    + "      \"essim:sending-institution\": {\"@id\": \"http://md.org/resource/insitutions#2800\"},\n"
                    + "      \"essim:references\": {\"@id\": \"_b1\"},\n"
                    + "      \"essim:mandate\": {\"@id\": \"_b2\"},\n"
                    + "      \"essim:scope\": {\"@id\": \"http://md.org/resource/categories#21006\", \"essim:scope\": \"Old Age pensions\"},\n"
                    + "      \"essim:creator-institution\": {\"@id\": \"http://md.org/resource/institutions#3656\"},\n"
                    + "      \"essim:dataset\": {\"@id\": \"http://www.w3c.org/ns/person#placeOfBirth\", \"essim:dataset\": \"Place of birth\"},\n"
                    + "      \"essim:format\": \"alphanumeric\",\n"
                    + "      \"essim:dataname\": {\"@id\": \"_b3\"},\n"
                    + "      \"essim:date\": {\"@id\": \"_b6\"},\n"
                    + "      \"essim:datacode\": {\"@id\": \"http://got.org/resource#IC007\", \"essim:datacode\":\"IC007\"}\n"
                    + "    },\n"
                    + "    {\n"
                    + "       \"@type\": \"dc:relation\",\n"
                    + "      \"@id\": \"_b0\",\n"
                    + "       \"essim:rel-description\": \"At least one nationality must match the place ot birth\",\n"
                    + "      \"essim:datacode\": {\"@id\":\"http://got.org/resource#IC009\", \"essim:datacode\": \"IC009 (Nationality)\"}\n"
                    + "    	\n"
                    + "  	},\n"
                    + "    {\n"
                    + "      \"@type\": \"dc:references\",\n"
                    + "      \"@id\": \"_b1\",\n"
                    + "      \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_2\": {\"@context\":{\"@language\":\"es\"}, \"@id\": \"http://social-security-forms.org/resource#IAFBIB1\", \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_2\":  \"IAFBIB1 Formulario de Enlace (IV. Lugar de nacimiento/ Región/Pais)\"}\n"
                    + "    },\n"
                    + "    {\n"
                    + "      \"@id\": \"_b2\",\n"
                    + "      \"@type\": \"rdf:Seq\",\n"
                    + "      \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_2\": {\"@context\":{\"@language\":\"es\"},\"@id\":\"http://eurlex.org/resource/regulations#32009R0987\", \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_2\": \"IAFBIB1 Formulario de Enlace (IV. Lugar de nacimiento/ Región/Pais)\"},\n"
                    + "      \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_1\": {\"@id\": \"http://eurlex.org/resource/regulations#32004R0883\", \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_1\": \"Articles 1, 6, 45, 49, 51, 52, 57, 60 of Regulation (EC) No 883/2004\"}\n"
                    + "      \n"
                    + "  	},    \n"
                    + "    {\n"
                    + "      \"@id\": \"_b3\",\n"
                    + "      \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_2\": {\"@context\":{\"@language\":\"es\"},\"@id\":\"http://got.org/resource#IC007_SP\", \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_2\": \"Lugar de nacimiento\"},\n"
                    + "      \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_1\": {\"@context\":{\"@language\":\"en\"}, \"@id\": \"http://got.org/resource#IC007\", \"http://www.w3.org/1999/02/22-rdf-syntax-ns#_1\": \"Place of birth\"}\n"
                    + "      \n"
                    + "      \n"
                    + "  	},\n"
                    + "    {\n"
                    + "      \"@id\": \"http://md.org/resource/insitutions#2800\",\n"
                    + "      \"essim:name\": \"Social Institute for Mariners\",\n"
                    + "      \"essim:code\": \"3656\"\n"
                    + "  	},\n"
                    + "    {\n"
                    + "      \"@id\": \"http://md.org/resource/institutions#3656\",\n"
                    + "      \"essim:name\": \"National Institute of Social Security Provincial Head Office of Madrid\",\n"
                    + "      \"essim:code\": \"2800\"\n"
                    + "  	},\n"
                    + "    {\n"
                    + "      \"@id\": \"_b6\",\n"
                    + "      \"essim:modified\": {\"@type\": \"http://www.w3.org/2001/XMLSchema#date\", \"@value\":\"2015-02-24\"},\n"
                    + "      \"essim:created\": {\"@type\": \"http://www.w3.org/2001/XMLSchema#date\", \"@value\":\"2014-05-30\"}\n"
                    + "  	}\n"
                    + "    \n"
                    + "  ]\n"
                    + "  \n"
                    + "}";
            return Response.ok(res).status(Response.Status.OK).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            ec.edu.utpl.eessi.util.Response res = new ec.edu.utpl.eessi.util.Response();
            res.setEessim_error_desc("FAIL: Bad request");
            res.setValue("NULL");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path(value = "/validation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validation(@QueryParam("uriws2") String uri) {
        System.out.println(uri + "*********************");
        String jsonld = new UtilEessi().getResource(uri);
        System.out.println(jsonld + "*********************");
        try {
            // Primeramente se procesa el json que se recibe como parámetro y se obtiene el datacode
            String datacode = "IC009845";
            //Se especifica el sparql endpoint a usar
            String sparqlEndpoint = "http://sparql.org/sparql";
            //Se define la sparql query
            // Mediante rdfField se especifica la ubicación física del archivo rdf con los 15 metadatos
            // Mediante rdfGOT se espcifica la ubicación física del archivo rdf del GOT
            String sparqlQuery = "PREFIX essim:<http://essim.org/ns/essim#>\n"
                    + "PREFIX got:<http://essim.org/ns/got#>\n"
                    + "\n"
                    + "ASK\n"
                    + "FROM <http://www.cc.uah.es/hilera/essim/got-resource.rdf>\n"
                    + "\n"
                    + "{\n"
                    + "?x got:column4 ?z2 .\n"
                    + "?x got:column2 ?z3 .\n"
                    + "FILTER regex (?z2, \""+datacode+"\") .\n"
                    + "FILTER regex (?z3, \"GENERIC\") .\n"
                    + "}";

            System.out.println(sparqlQuery);
            // Se crea un objeto query para ejecutar la consulta
            Query query = QueryFactory.create(sparqlQuery, Syntax.syntaxARQ);
            // httpQuery permite la ejecución de consultas desde servicios remotos
            QueryEngineHTTP httpQuery = new QueryEngineHTTP(sparqlEndpoint, query);
            // Se ejecuta la consulta en este caso se llama al método execAsk() que devuelve un valor boleano
            Boolean ask = httpQuery.execAsk();
            ec.edu.utpl.eessi.util.Response res = new ec.edu.utpl.eessi.util.Response();
            res.setValue(ask.toString());
            if (!ask) {
                res.setEessim_error_code("34");
                res.setEessim_error_desc("datacocode metadata does not match with a generic term in Glossary of terms");
            }
            return Response.ok(res).status(Response.Status.OK).build();
        } catch (Exception ex) {
            ex.printStackTrace();
            ec.edu.utpl.eessi.util.Response res = new ec.edu.utpl.eessi.util.Response();
            res.setEessim_error_desc("FAIL: Bad request");
            res.setValue("NULL");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
