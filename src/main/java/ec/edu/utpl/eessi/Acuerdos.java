/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author utpl
 */
@Entity
@Table(name = "acuerdos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acuerdos.findAll", query = "SELECT a FROM Acuerdos a"),
    @NamedQuery(name = "Acuerdos.findById", query = "SELECT a FROM Acuerdos a WHERE a.id = :id"),
    @NamedQuery(name = "encontrarAcuerdos", query = "SELECT a FROM Acuerdos a WHERE a.acuerdo = :acuerdo")})
public class Acuerdos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 65535)
    @Column(name = "acuerdo")
    private String acuerdo;
    @Lob
    @Size(max = 65535)
    @Column(name = "tecnologia")
    private String tecnologia;
    @Lob
    @Size(max = 65535)
    @Column(name = "url_servicio")
    private String urlServicio;

    public Acuerdos() {
    }

    public Acuerdos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAcuerdo() {
        return acuerdo;
    }

    public void setAcuerdo(String acuerdo) {
        this.acuerdo = acuerdo;
    }

    public String getTecnologia() {
        return tecnologia;
    }

    public void setTecnologia(String tecnologia) {
        this.tecnologia = tecnologia;
    }

    public String getUrlServicio() {
        return urlServicio;
    }

    public void setUrlServicio(String urlServicio) {
        this.urlServicio = urlServicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acuerdos)) {
            return false;
        }
        Acuerdos other = (Acuerdos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.edu.utpl.eessi.Acuerdos[ id=" + id + " ]";
    }
    
}
