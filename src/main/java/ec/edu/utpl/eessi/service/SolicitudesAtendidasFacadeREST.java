/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi.service;

import ec.edu.utpl.eessi.SolicitudesAtendidas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author utpl
 */
@Stateless
@Path("reply")
public class SolicitudesAtendidasFacadeREST extends AbstractFacade<SolicitudesAtendidas> {
    @PersistenceContext(unitName = "ec.edu.utpl_eessi_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public SolicitudesAtendidasFacadeREST() {
        super(SolicitudesAtendidas.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(SolicitudesAtendidas entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, SolicitudesAtendidas entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public SolicitudesAtendidas find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<SolicitudesAtendidas> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<SolicitudesAtendidas> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("replyPensionApplication")
    @Produces(MediaType.APPLICATION_JSON)
    public Response solicitudPension(@QueryParam("ticket") Long ticket,
           @QueryParam("res") String res,
           @QueryParam("ecr1") String ecr1,
           @QueryParam("ecr2") String ecr2) {
        
        Message m = new Message();
        try {
//            JSONObject json = new JSONObject(res);
//            String campo1 = json.getString("campo_1");
//            String campo2 = json.getString("campo_2");
//            SolicitudesAtendidas sa = new SolicitudesAtendidas();
//            sa.setCampo1(campo1);
//            sa.setCampo2(campo2);
//            sa.setTicket(ticket);
//            sa.setMetaCampo1(ecr1);
//            sa.setMetaCampo2(ecr2);
            //super.create(sa);
            m.setMessage("Request satisfactorily addressed");
        } catch (Exception ex) {
            System.err.println(ex.getCause());
            ex.printStackTrace();
        }
        return Response.ok(m).build();
    }
    
    
}
