/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi.service;

import com.google.gson.Gson;
import ec.edu.utpl.eessi.Acuerdos;
import ec.edu.utpl.eessi.util.Ticket;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author utpl
 */
@Stateless
@Path("/agreements")
public class AcuerdosFacadeREST extends AbstractFacade<Acuerdos> {
    @PersistenceContext(unitName = "ec.edu.utpl_eessi_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public AcuerdosFacadeREST() {
        super(Acuerdos.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Acuerdos entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Acuerdos entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Acuerdos find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Acuerdos> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Acuerdos> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("/verifyingAgreement")
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyngAgreements(@QueryParam("agreement") String a,
            @DefaultValue("c") @QueryParam("callback") String callback) {
        Acuerdos acuerdo = null;//(Acuerdos) em.createNamedQuery("encontrarAcuerdos").setParameter("acuerdo", a).getSingleResult();
        System.out.println("*****************");
        //System.out.println(acuerdo.getAcuerdo());
        if (acuerdo != null) {
            if (callback.equals("c")) {
                return Response.ok(Boolean.TRUE).build();
            } else {
                Gson gson = new Gson();
                //return Response.ok(callback + "(" + gson.toJson(acuerdo) + ")").status(Response.Status.OK).build();
                return Response.ok(callback + "(" + gson.toJson(Boolean.TRUE) + ")").status(Response.Status.OK).build();
            }
        } else {
            ec.edu.utpl.eessi.util.Response res = new ec.edu.utpl.eessi.util.Response();
            res.setEessim_error_code("2045");
            res.setEessim_error_desc("No exists agreement");
            res.setValue("false");
            return Response.ok(res).build();
        }
    }
    
    
}
