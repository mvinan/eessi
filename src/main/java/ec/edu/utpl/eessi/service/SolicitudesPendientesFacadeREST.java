/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi.service;

import ec.edu.utpl.eessi.SolicitudesPendientes;
import ec.edu.utpl.eessi.util.Ticket;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author utpl
 */
@Stateless
@Path("appealsProcess")
public class SolicitudesPendientesFacadeREST extends AbstractFacade<SolicitudesPendientes> {
    @PersistenceContext(unitName = "ec.edu.utpl_eessi_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public SolicitudesPendientesFacadeREST() {
        super(SolicitudesPendientes.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(SolicitudesPendientes entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Long id, SolicitudesPendientes entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public SolicitudesPendientes find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<SolicitudesPendientes> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<SolicitudesPendientes> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("pensionApplication/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response solicitudPension(@QueryParam("data") String data,
            @QueryParam("ec1") String ec1,
            @QueryParam("ec2") String ec2,
            @QueryParam("ec3") String ec3) {
        long ticket = 1l;
        Ticket t = new Ticket();
        try {
            //JSONObject json = new JSONObject(data);
//            String campo1 = json.getString("campo_1");
//            String campo2 = json.getString("campo_2");
//            String campo3 = json.getString("campo_3");
            Calendar c = Calendar.getInstance();
            ticket = c.getTimeInMillis();
            Random rand = new Random();
            long n = rand.nextInt(5000) + 1;
//            SolicitudesPendientes entity = new SolicitudesPendientes();
//            ticket = ticket + n;
//            entity.setTicket(ticket);
////            entity.setNombres(campo2);
////            entity.setApellidos(campo3);
////            entity.setInstitutionName(campo1);
//            entity.setMetaApellidos(ec3);
//            entity.setMetaInstitutionName(ec1);
//            entity.setMetaNombres(ec2);
            //super.create(entity);
            t.setTicket(ticket);
        } catch (Exception ex) {
            System.err.println(ex.getCause());
            ex.printStackTrace();
        }
        return Response.ok(t).build();
    }
    
}
