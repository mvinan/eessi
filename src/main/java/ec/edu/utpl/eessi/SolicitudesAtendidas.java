/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author utpl
 */
@Entity
@Table(name = "solicitudes_atendidas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudesAtendidas.findAll", query = "SELECT s FROM SolicitudesAtendidas s"),
    @NamedQuery(name = "SolicitudesAtendidas.findById", query = "SELECT s FROM SolicitudesAtendidas s WHERE s.id = :id"),
    @NamedQuery(name = "SolicitudesAtendidas.findByTicket", query = "SELECT s FROM SolicitudesAtendidas s WHERE s.ticket = :ticket")})
public class SolicitudesAtendidas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 65535)
    @Column(name = "campo1")
    private String campo1;
    @Lob
    @Size(max = 65535)
    @Column(name = "campo2")
    private String campo2;
    @Lob
    @Size(max = 65535)
    @Column(name = "meta_campo1")
    private String metaCampo1;
    @Column(name = "ticket")
    private Long ticket;
    @Lob
    @Size(max = 65535)
    @Column(name = "meta_campo2")
    private String metaCampo2;
    

    public SolicitudesAtendidas() {
    }

    public SolicitudesAtendidas(Integer id) {
        this.id = id;
    }
    
    public Long getTicket() {
        return ticket;
    }

    public void setTicket(Long ticket) {
        this.ticket = ticket;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCampo1() {
        return campo1;
    }

    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    public String getCampo2() {
        return campo2;
    }

    public void setCampo2(String campo2) {
        this.campo2 = campo2;
    }

    public String getMetaCampo1() {
        return metaCampo1;
    }

    public void setMetaCampo1(String metaCampo1) {
        this.metaCampo1 = metaCampo1;
    }

    
    public String getMetaCampo2() {
        return metaCampo2;
    }

    public void setMetaCampo2(String metaCampo2) {
        this.metaCampo2 = metaCampo2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudesAtendidas)) {
            return false;
        }
        SolicitudesAtendidas other = (SolicitudesAtendidas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.edu.utpl.eessi.SolicitudesAtendidas[ id=" + id + " ]";
    }
    
}
