/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.eessi;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author utpl
 */
@Entity
@Table(name = "solicitudes_pendientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudesPendientes.findAll", query = "SELECT s FROM SolicitudesPendientes s"),
    @NamedQuery(name = "SolicitudesPendientes.findByTicket", query = "SELECT s FROM SolicitudesPendientes s WHERE s.ticket = :ticket")})
public class SolicitudesPendientes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ticket")
    private Long ticket;
    @Lob
    @Size(max = 65535)
    @Column(name = "institution_name")
    private String institutionName;
    @Lob
    @Size(max = 65535)
    @Column(name = "nombres")
    private String nombres;
    @Lob
    @Size(max = 65535)
    @Column(name = "apellidos")
    private String apellidos;
    @Lob
    @Size(max = 65535)
    @Column(name = "meta_institution_name")
    private String metaInstitutionName;
    @Lob
    @Size(max = 65535)
    @Column(name = "meta_nombres")
    private String metaNombres;
    @Lob
    @Size(max = 65535)
    @Column(name = "meta_apellidos")
    private String metaApellidos;

    public SolicitudesPendientes() {
    }

    public SolicitudesPendientes(Long ticket) {
        this.ticket = ticket;
    }

    public Long getTicket() {
        return ticket;
    }

    public void setTicket(Long ticket) {
        this.ticket = ticket;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getMetaInstitutionName() {
        return metaInstitutionName;
    }

    public void setMetaInstitutionName(String metaInstitutionName) {
        this.metaInstitutionName = metaInstitutionName;
    }

    public String getMetaNombres() {
        return metaNombres;
    }

    public void setMetaNombres(String metaNombres) {
        this.metaNombres = metaNombres;
    }

    public String getMetaApellidos() {
        return metaApellidos;
    }

    public void setMetaApellidos(String metaApellidos) {
        this.metaApellidos = metaApellidos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ticket != null ? ticket.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudesPendientes)) {
            return false;
        }
        SolicitudesPendientes other = (SolicitudesPendientes) object;
        if ((this.ticket == null && other.ticket != null) || (this.ticket != null && !this.ticket.equals(other.ticket))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.edu.utpl.eessi.SolicitudesPendientes[ ticket=" + ticket + " ]";
    }
    
}
